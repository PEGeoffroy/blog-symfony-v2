<?php

namespace App\Entity;

class Article
{
    public $id;
    public $title;
    public $lead_paragraph;
    public $content;

    public function fromSQL(array $rawData) {
        $this->id = $rawData["id"];
        $this->title = $rawData["title"];
        $this->lead_paragraph = $rawData["lead_paragraph"];
        $this->content = $rawData["content"];
    }
}