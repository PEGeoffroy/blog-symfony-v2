<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ArticleType;

class EditionController extends Controller
{
    /**
     * @Route("/admin/article/{id}", name="edition")
     */
    public function index(int $id, Request $request, ArticleRepository $repo)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("admin");
        }

        return $this->render('admin/edition.html.twig', [
            "form" => $form->createView(),
            'subtitle' => "Edition",
            "article" => $article
        ]);
        }
}