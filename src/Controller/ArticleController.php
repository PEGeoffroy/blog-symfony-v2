<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
// use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    /**
     * @Route("/article/{id}", name="article")
     */
    public function index(int $id, ArticleRepository $repo)
    {
        $result = $repo->getById($id);
        return $this->render('public/article.html.twig', [
            'result'  => $result,
            'subtitle' => $result->title
        ]);
    }

    /**
     * @Route("/admin/remove/{id}", name="remove_article")
     */

    public function remove(int $id, ArticleRepository $repo){
        $repo->delete($id);
        return $this->redirectToRoute("admin");
    }
}
