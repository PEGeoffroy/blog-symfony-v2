<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;

class ReceptionController extends Controller
{
    /**
     * @Route("/", name="reception")
     */
    public function index(ArticleRepository $repo)
    {
        $result = $repo->getAll();
        return $this->render('public/index.html.twig', [
            'result'  => $result,
            'subtitle' => "Reception"
        ]);
    }
}
